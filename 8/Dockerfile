# from http://typo3.org/typo3-cms/overview/requirements/
FROM php:7.1.3-apache

# locale and zoneinfo
ENV LOCALE=de_DE.UTF-8
ENV LOCALTIME=Europe/Berlin

RUN a2enmod rewrite \
	&& a2enmod deflate

# install the PHP extensions we need
RUN apt-get update && apt-get install -y locales libpng12-dev libjpeg-dev libpq-dev libxml2-dev freetype* graphicsmagick zip \
	&& sed -i "s/^# *\(${LOCALE}\)/\1/" /etc/locale.gen && locale-gen \
	&& ln -f -s /usr/share/zoneinfo/${LOCALTIME} /etc/localtime \
	&& rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr --with-freetype-dir=/usr \
	&& docker-php-ext-install gd mbstring opcache soap mysqli zip

# change system default language
ENV LANG=${LOCALE}

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/01-opcache-recommended.ini

# see https://github.com/TYPO3/TYPO3.CMS/blob/TYPO3_8-0/INSTALL.md
RUN { \
		echo 'upload_max_filesize=100M'; \
		echo 'post_max_size=100M'; \
		echo 'max_execution_time=240'; \
		echo 'memory_limit=128M'; \
		echo 'max_input_vars=1500'; \
		echo 'opcache.save_comments=1'; \
		echo 'opcache.load_comments=1'; \
		echo 'opcache.validate_timestamps=0'; \
	} > /usr/local/etc/php/conf.d/02-typo3-recommended.ini

WORKDIR /var/www

# http://typo3.org/download/
ENV TYPO3_VERSION 8.7.2
ENV TYPO3_MD5 084a336f7651671774d71ecd5ce298a6

RUN curl -fSL "http://get.typo3.org/${TYPO3_VERSION}" -o typo3_src.tar.gz \
	&& echo "${TYPO3_MD5} *typo3_src.tar.gz" | md5sum -c - \
	&& tar -xz -f typo3_src.tar.gz \
	&& rm typo3_src.tar.gz

WORKDIR /var/www/html

RUN ln -s ../typo3_src-${TYPO3_VERSION} typo3_src \
	&& ln -s typo3_src/typo3 \
	&& ln -s typo3_src/index.php \
	&& cp typo3_src/_.htaccess .htaccess \
	&& touch FIRST_INSTALL \
	&& chown -R www-data:www-data .

# cleanup
RUN apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*